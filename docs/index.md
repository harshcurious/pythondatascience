# Welcome to Python Data Science

**Python Data Science** is an open source, collaborative project aiming to document best practice approaches to data science tasks using Python. At present there are two main classes of resources:

* A collection of *Python Task Views*
* The *Jupyter Overview* that compares Python functionality against the R and Julia data science frameworks

## Python Task Views

[Task Views](https://www.pythondatascience.org/) is a collection of documents in markdown format that provide guidance on which python packages are relevant for which data science task. 

Task views aim to outline which packages could be included (or excluded) in a certain project to achieve certain functionality. They are *not* meant to endorse the "best" package for any given task. 

The initial proposed list of python task views mirrors the CRAN set of corresponding Task Views for the R system. Over time this may develop to reflect more accurately the grouping of tasks in the Python universe


### Python Datascience Taskview List

* [Analysis of GeoSpatial Data](Geospatial.md)
* Analysis of Pharmacokinetic Data
* Analysis of Ecological and Environmental Data
* Bayesian Inference
* Chemometrics and Computational Physics
* Clinical Trial Design, Monitoring, and Analysis
* Cluster Analysis & Finite Mixture Models
* [Databases](Databases.md)
* Differential Equations
* [Econometrics](Econometrics.md)
* Design of Experiments (DoE) & Analysis of Experimental Data
* Extreme Value Analysis
* Empirical Finance
* Functional Data Analysis
* Graphical Models in Python
* Handling and Analyzing Spatio-Temporal Data
* [High-Performance and Parallel Computing with Python](HPC.md)
* Hydrological Data and Modeling
* [Machine Learning & Statistical Learning](MachineLearning.md)
* Medical Image Analysis
* Meta-Analysis
* Missing Data
* Model Deployment with Python
* [Multivariate Statistics](Statistics.md)
* Natural Language Processing
* [Numerical Mathematics](Mathematics.md)
* Official Statistics & Survey Methodology
* Optimization and Mathematical Programming
* Phylogenetics, Especially Comparative Methods
* Probability Distributions
* Psychometric Models and Methods
* [Regression](Regression.md)  
* [Reproducible Research](Reproducibility.md)
* Robust Statistical Methods
* [Semantic Data](SemanticData.md)
* Statistics for the Social Sciences
* Statistical Genetics
* Survival Analysis
* Teaching Statistics
* Time Series Analysis
* [Visualization](Visualization.md)
* [Web Technologies and Services](Web.md)


## Jupyter (Python versus R versus Julia) overview

While Task Views are dedicated exclusively to Python data science tools, the Jupyter overview project is inspired by the R ecosystem [CRAN views](https://cran.r-project.org/web/views/). It offers a side-by-side comparison with R and Julia packages available for data science helps identify important sub-domains where Python may currently lag. The overview is available in two formats:

* As a wiki page: [Jupyter Overview Wiki](https://www.openriskmanual.org/wiki/Overview_of_the_Julia-Python-R_Universe). 
* As a markdown document: [Jupyter Overview Markdown](Jupyter.md)