## About Python Data Science


### How Does it Work?

Each Python Task View document lives in a separate file hosted in this [repository](https://gitlab.com/openrisk/pythondatascience). 

The files are automatically displayed in a [Gitlab Pages website](https://www.pythondatascience.org/)

### Moderators

Anybody who has good knowledge of python data science tools used in a specific domain is welcome to contribute to the knowledge base. Similar to CRAN, we want to have a small number of moderators per topic to help organize the content and ensure it is a high quality resource that adds value to all users.